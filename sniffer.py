import socket
import struct


# Unpack ethernet frame
def ethernet_frame(data):
    dest_mac, src_mac, proto = struct.unpack('!6s6s2s', data[:14])
    return get_mac_addr(dest_mac), get_mac_addr(src_mac), get_protocol(proto), data[14:]

# Return formatted MAC address AA:BB:CC:DD:EE:FF
def get_mac_addr(bytes_addr):
    bytes_str = map('{:02x}'.format, bytes_addr)
    mac_address = ':'.join(bytes_str).upper()
    return mac_address

# Return formatted protocol ABCD
def get_protocol(bytes_proto):
    bytes_str = map('{:02x}'.format, bytes_proto)
    protocol = ''.join(bytes_str).upper()
    return protocol

def ipv4_frame(data):
    version_header_length = data[0]
    version = version_header_length >> 4
    header_length = (version_header_length & 15) * 4
    ttl, proto, src, target = struct.unpack('! 8x B B 2x 4s 4s',data[:20])
    return version,header_length,ttl,proto,src,target

def tcp_head(data):
    src_port, dest_port, seq, ack, offset_reserved_flags = struct.unpack('! H H L L H',data[:14])
    offset = (offset_reserved_flags >> 12) * 4
    flag_urg = (offset_reserved_flags & 32) >> 5
    flag_ack = (offset_reserved_flags & 16) >> 4
    flag_psh = (offset_reserved_flags & 8) >> 3
    flag_rst = (offset_reserved_flags & 4) >> 2
    flag_syn = (offset_reserved_flags & 2) >> 1
    flag_fin = offset_reserved_flags & 1
    s_data = data[offset:]
    return src_port, dest_port, seq, ack, flag_urg, flag_ack, flag_psh, flag_rst, flag_syn, flag_fin, s_data

def icmp_head(data):
    i_type, code = struct.unpack('! B B 2x',data[:4])
    i_data=data[4:]
    return i_type, code, i_data

def udp_head(data):
    src_port, dest_port, len = struct.unpack('! H H H 2x',data[:8])
    u_data = data[8:]
    return src_port, dest_port, len, u_data

s = socket.socket(socket.AF_PACKET, socket.SOCK_RAW, socket.ntohs(3))
while True:
    raw_data, addr = s.recvfrom(65535)
    dest_mac, src_mac, eth_protocol, data = ethernet_frame(raw_data)
    print('\nEthernet Frame:')
    print("Destination MAC: {}".format(dest_mac))
    print("Source MAC: {}".format(src_mac))
    print("EtherType: {}".format(eth_protocol))
    if eth_protocol == "0800":
        version,header_length,ttl,ip_protocol,src,target=ipv4_frame(raw_data[14:34])
        print("Version: {}".format(version))
        print("Header length: {}".format(header_length))
        print("TTL: {}".format(ttl))
        print("Protocol type: {}".format(ip_protocol))
        print("Src IP: {}".format('.'.join(map(str,src))))
        print("Dst IP: {}".format('.'.join(map(str,target))))
        if ip_protocol == 6:
            src_port, dest_port, seq, ack, flag_urg, flag_ack, flag_psh, flag_rst, flag_syn, flag_fin, s_data = tcp_head(raw_data[34:])
            print("Src Port: {}".format(src_port))
            print("Dst Port: {}".format(dest_port))
            print("Seq nr: {}".format(seq))
            print("Ack nu: {}".format(ack))
            print("URG: {}, ACK: {}, PSH: {}, RST: {}, SYN: {}, FIN: {}".format(flag_urg, flag_ack, flag_psh, flag_rst, flag_syn, flag_fin))
            print(s_data.decode("utf-8", errors='ignore'))
        if ip_protocol == 1:
            i_type, code, i_data = icmp_head(raw_data[34:])
            print("Type: {}".format(i_type))
            print("Code: {}".format(code))
            print(i_data.decode("utf-8", errors='ignore'))
        if ip_protocol == 17:
            src_port, dest_port, len, u_data = udp_head(raw_data[34:])
            print("Src Port: {}".format(src_port))
            print("Dst Port: {}".format(dest_port))
            print("Length: {}".format(len))
            print(u_data.decode("utf-8", errors='ignore'))



